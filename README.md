A small puzzle game made written in c++ using OpenGL API, in which the player must find different objects hidden in the scene.

**Deployment**
## **Exectue**

Clone project and compile the C++ code in your compiler of preference, and then run the executable.

**Basic Movement List**
*  W		/	Move Forward
*  A		/	Move Left
*  S 	    / 	Move Backwards
*  D		/	Move Right
*  E		/	Toggle Flashlight
*  Mouse    /   Move the Camera View Point

