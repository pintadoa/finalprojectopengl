#include "GLWindow.h"

GLWindow::GLWindow()
{
	width = 1024;
	height = 768;

	// Initialize key input arrays
	for (size_t i = 0; i < 1024; i++)
	{
		keys[i] = 0;
	}
}

GLWindow::GLWindow(GLint windowWidth, GLint windowHeight)
{
	width = windowWidth;
	height = windowHeight;

	// Initialize key input arrays
	for (size_t i = 0; i < 1024; i++)
	{
		keys[i] = 0;
	}
}

GLWindow::~GLWindow()
{
	glfwDestroyWindow(mainWindow);
	glfwTerminate();
}

int GLWindow::init()
{
	if (!glfwInit())
	{
		printf("Error Initialising GLFW");
		glfwTerminate();
		return 1;
	}

	// Setup GLFW Windows Properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);


	// Create the window
	mainWindow = glfwCreateWindow(width, height, "Test Window", NULL, NULL);
	if (!mainWindow)
	{
		printf("Error creating GLFW window!");
		glfwTerminate();
		return 1;
	}

	// Get buffer size information
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	// Set the current context
	glfwMakeContextCurrent(mainWindow);

	// Handle Key + Mouse Input
	createCallbacks();
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		printf("Error: %s", glewGetErrorString(error));
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}

	glEnable(GL_DEPTH_TEST);

	// Create Viewport
	glViewport(0, 0, bufferWidth, bufferHeight);

	glfwSetWindowUserPointer(mainWindow, this);
}

void GLWindow::createCallbacks()
{
	glfwSetKeyCallback(mainWindow, handleKeysPress);
	glfwSetCursorPosCallback(mainWindow, handleMouseMovement);
}

GLfloat GLWindow::getXMovement()
{
	GLfloat movement = xMovement;
	xMovement = 0.0f;
	return movement;
}

GLfloat GLWindow::getYMovement()
{
	GLfloat movement = yMovement;
	yMovement = 0.0f;
	return movement;
}

void GLWindow::handleKeysPress(GLFWwindow* window, int key, int code, int action, int mode)
{
	// Get the user pointer of main window
	GLWindow* theWindow = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			theWindow->keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			theWindow->keys[key] = false;
		}
	}
}

void GLWindow::handleMouseMovement(GLFWwindow* window, double xPos, double yPos)
{
	// Get the user pointer of main window
	GLWindow* theWindow = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));

	// Check if its the first movement
	if (theWindow->mouseFirstMoved)
	{
		theWindow->lastX = xPos;
		theWindow->lastY = yPos;
		theWindow->mouseFirstMoved = false;
	}

	// Calculate what the mouse movement was in x and y
	theWindow->xMovement = xPos - theWindow->lastX;
	theWindow->yMovement = theWindow->lastY - yPos;

	// Set to the last x and y value to the current position
	theWindow->lastX = xPos;
	theWindow->lastY = yPos;
}
