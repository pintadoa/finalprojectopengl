#pragma once
#ifndef COMMONVALS
#define COMMONVALS
#include "stb_image.h"
const int MAX_POINT_LIGHTS = 9;
const int MAX_SPOT_LIGHTS = 9;
#endif