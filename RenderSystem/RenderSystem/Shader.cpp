#include "Shader.h"

Shader::Shader()
{
	shaderID = 0;
	uniformModel = 0;
	uniformProjection = 0;

	pointLightCount = 0;
	spotLightCount = 0;
}

Shader::~Shader()
{
	ClearShader();
}

void Shader::CreateFromFiles(const char* vertexLocation, const char* fragmentLocation)
{
	std::string vertexString = ReadFile(vertexLocation);
	std::string fragmentString = ReadFile(fragmentLocation);
	const char* vertexCode = vertexString.c_str();
	const char* fragmentCode = fragmentString.c_str();

	CompileShader(vertexCode, fragmentCode);
}

std::string Shader::ReadFile(const char* fileLocation)
{
	std::string content;
	std::ifstream fileStream(fileLocation, std::ios::in);

	if (!fileStream.is_open()) {
		printf("Failed to read %s! File doesn't exist.", fileLocation);
		return "";
	}

	std::string line = "";
	while (!fileStream.eof())
	{
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}

void Shader::CompileShader(const char* vertexCode, const char* fragmentCode)
{
	shaderID = glCreateProgram();

	if (!shaderID)
	{
		printf("Error creating shader program!\n");
		return;
	}

	AddShader(shaderID, vertexCode, GL_VERTEX_SHADER);
	AddShader(shaderID, fragmentCode, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	glLinkProgram(shaderID);
	glGetProgramiv(shaderID, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shaderID, sizeof(eLog), NULL, eLog);
		printf("Error linking program: '%s'\n", eLog);
		return;
	}

	glValidateProgram(shaderID);
	glGetProgramiv(shaderID, GL_VALIDATE_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(shaderID, sizeof(eLog), NULL, eLog);
		printf("Error validating program: '%s'\n", eLog);
		return;
	}

	uniformProjection = glGetUniformLocation(shaderID, "projection");
	uniformModel = glGetUniformLocation(shaderID, "model");
	uniformView = glGetUniformLocation(shaderID, "view");
	uniformDirectionalLight.uniformColour = glGetUniformLocation(shaderID, "directionalLight.base.colour");
	uniformDirectionalLight.uniformAmbientIntensity = glGetUniformLocation(shaderID, "directionalLight.base.ambientIntensity");
	uniformDirectionalLight.uniformDirection = glGetUniformLocation(shaderID, "directionalLight.direction");
	uniformDirectionalLight.uniformDiffuseIntensity = glGetUniformLocation(shaderID, "directionalLight.base.diffuseIntensity");
	uniformSpecularIntensity = glGetUniformLocation(shaderID, "material.specularIntensity");
	uniformShininess = glGetUniformLocation(shaderID, "material.shininess");
	uniformEyePosition = glGetUniformLocation(shaderID, "eyePosition");

	uniformPointLightCount = glGetUniformLocation(shaderID, "pointLightCount");

	// Loop trhough all the point lights
	for (size_t i = 0; i < MAX_POINT_LIGHTS; i++)
	{
		// Set char array 
		char locBuff[100] = { '\0' };

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].base.colour", i);
		// Get the location 
		uniformPointLight[i].uniformColour = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].base.ambientIntensity", i);
		// Get the location 
		uniformPointLight[i].uniformAmbientIntensity = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].base.diffuseIntensity", i);
		// Get the location 
		uniformPointLight[i].uniformDiffuseIntensity = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].position", i);
		// Get the location 
		uniformPointLight[i].uniformPosition = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].constant", i);
		// Get the location 
		uniformPointLight[i].uniformConstant = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].linear", i);
		// Get the location 
		uniformPointLight[i].uniformLinear = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "pointLights[%d].exponent", i);
		// Get the location 
		uniformPointLight[i].uniformExponent = glGetUniformLocation(shaderID, locBuff);
	}

	uniformSpotLightCount = glGetUniformLocation(shaderID, "spotLightCount");

	// Loop trhough all the point lights
	for (size_t i = 0; i < MAX_SPOT_LIGHTS; i++)
	{
		// Set char array 
		char locBuff[100] = { '\0' };

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.base.colour", i);
		// Get the location 
		uniformSpotLight[i].uniformColour = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.base.ambientIntensity", i);
		// Get the location 
		uniformSpotLight[i].uniformAmbientIntensity = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.base.diffuseIntensity", i);
		// Get the location 
		uniformSpotLight[i].uniformDiffuseIntensity = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.position", i);
		// Get the location 
		uniformSpotLight[i].uniformPosition = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.constant", i);
		// Get the location 
		uniformSpotLight[i].uniformConstant = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.linear", i);
		// Get the location 
		uniformSpotLight[i].uniformLinear = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].base.exponent", i);
		// Get the location 
		uniformSpotLight[i].uniformExponent = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].direction", i);
		// Get the location 
		uniformSpotLight[i].uniformDirection = glGetUniformLocation(shaderID, locBuff);

		// Print to a buffer 
		snprintf(locBuff, sizeof(locBuff), "spotLights[%d].edge", i);
		// Get the location 
		uniformSpotLight[i].uniformEdge = glGetUniformLocation(shaderID, locBuff);
	}
}

void Shader::SetDirectionalLight(DirectionalLight * directionalLight)
{
	directionalLight->UseLight(uniformDirectionalLight.uniformAmbientIntensity, uniformDirectionalLight.uniformColour,
		uniformDirectionalLight.uniformDiffuseIntensity, uniformDirectionalLight.uniformDirection);
}

void Shader::SetPointLights(PointLight * pointLight, unsigned int lightCount)
{
	// Check lightcount to see if it has more than the max 
	if (lightCount > MAX_POINT_LIGHTS)
	{
		// Limit to Max 
		lightCount = MAX_POINT_LIGHTS;
	}

	// Binding light variable
	glUniform1i(uniformPointLightCount, lightCount);

	for (auto i = 0; i < lightCount; i++)
	{
		pointLight[i].UseLight(uniformPointLight[i].uniformAmbientIntensity, uniformPointLight[i].uniformColour,
			uniformPointLight[i].uniformDiffuseIntensity, uniformPointLight[i].uniformPosition,
			uniformPointLight[i].uniformConstant, uniformPointLight[i].uniformLinear, uniformPointLight[i].uniformExponent);
	}
}

void Shader::SetSpotLights(SpotLight * spotLight, unsigned int lightCount)
{
	// Check lightcount to see if it has more than the max 
	if (lightCount > MAX_SPOT_LIGHTS)
	{
		// Limit to Max 
		lightCount = MAX_SPOT_LIGHTS;
	}

	// Binding light variable
	glUniform1i(uniformSpotLightCount, lightCount);

	for (auto i = 0; i < lightCount; i++)
	{
		spotLight[i].UseLight(uniformSpotLight[i].uniformAmbientIntensity, uniformSpotLight[i].uniformColour,
			uniformSpotLight[i].uniformDiffuseIntensity, uniformSpotLight[i].uniformPosition, uniformSpotLight[i].uniformDirection,
			uniformSpotLight[i].uniformConstant, uniformSpotLight[i].uniformLinear, uniformSpotLight[i].uniformExponent,
			uniformSpotLight[i].uniformEdge);
	}
}

void Shader::UseShader()
{
	glUseProgram(shaderID);
}

void Shader::ClearShader()
{
	if (shaderID != 0)
	{
		glDeleteProgram(shaderID);
		shaderID = 0;
	}

	uniformModel = 0;
	uniformProjection = 0;
}


void Shader::AddShader(GLuint theProgram, const char* shaderCode, GLenum shaderType)
{
	GLuint theShader = glCreateShader(shaderType);

	const GLchar* theCode[1];
	theCode[0] = shaderCode;

	GLint codeLength[1];
	codeLength[0] = strlen(shaderCode);

	glShaderSource(theShader, 1, theCode, codeLength);
	glCompileShader(theShader);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(theShader, sizeof(eLog), NULL, eLog);
		printf("Error compiling the %d shader: '%s'\n", shaderType, eLog);
		return;
	}

	glAttachShader(theProgram, theShader);
}


