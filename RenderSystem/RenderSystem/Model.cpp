#include "Model.h"


Model::Model()
{
	Position = new glm::vec3(0);
	Rotation = new glm::vec3(0);
	Scale = new glm::vec3(1);
}


Model::~Model()
{
}

void Model::LoadModel(const std::string & filename)
{
	Assimp::Importer importer;
	// Force to tri, Flip texture uvs along Y axis, smooth normals, Join verticies if they are at the same point, 
	const aiScene* scene = importer.ReadFile(filename, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices);

	if (!scene)
	{
		printf("Model (%s) failed to load: %s", filename, importer.GetErrorString());
		return;
	}

	LoadNode(scene->mRootNode, scene);
	LoadMaterials(scene);
}

void Model::RenderModel()
{
	// Loop through meshes
	for (size_t i = 0; i < meshList.size(); i++)
	{
		unsigned int materialIndex = meshToTex[i];
		// Check the material exists 
		if (materialIndex < textureList.size() && textureList[materialIndex])
		{
			textureList[materialIndex]->UseTexture();
		}

		meshList[i]->RenderMesh();
	}
}

void Model::ClearModel()
{
	// Loop through meshes
	for (auto i = 0; i < meshList.size(); i++)
	{
		if (meshList[i])
		{
			delete meshList[i];
			meshList[i] = nullptr;
		}
	}

	// Loop through textures
	for (auto i = 0; i < textureList.size(); i++)
	{
		if (textureList[i])
		{
			delete textureList[i];
			textureList[i] = nullptr;
		}
	}
}

void Model::LoadNode(aiNode * node, const aiScene * scene)
{
	// Loop through all of our meshes
	for (auto i = 0; i < node->mNumMeshes; i++)
	{
		// Load the mesh in the scene that holds the id 
		LoadMesh(scene->mMeshes[node->mMeshes[i]],scene);
	}

	for (auto i = 0; i < node->mNumChildren; i++)
	{
		// Recursively load the child nodes
		LoadNode(node->mChildren[i], scene);
	}
}

void Model::LoadMesh(aiMesh * mesh, const aiScene * scene)
{
	std::vector<GLfloat> vertices;
	std::vector<unsigned int> indices;

	// Loop through vertices
	for (auto i = 0; i < mesh->mNumVertices; i++)
	{
		// Insert X,Y,Z
		vertices.insert(vertices.end(), { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z });
		// Check if object has texture
		if (mesh->mTextureCoords[0])
		{
			// Insert U,V
			vertices.insert(vertices.end(), { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y });
		}
		else {
			// No texture
			vertices.insert(vertices.end(), { 0.0f, 0.0f });
		}
		// Insert nx ny nz
		vertices.insert(vertices.end(), { -mesh->mNormals[i].x, -mesh->mNormals[i].y, -mesh->mNormals[i].z });
	}

	// Loop through faces
	for (auto i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Loop through the points a face contains
		for (auto j = 0; j < face.mNumIndices; j++)
		{
			// Add them to the indices list
			indices.push_back(face.mIndices[j]);
		}
	}

	Mesh* newMesh = new Mesh();
	newMesh->CreateMesh(&vertices[0], &indices[0], vertices.size(), indices.size());
	meshList.push_back(newMesh);
	meshToTex.push_back(mesh->mMaterialIndex);
}

void Model::LoadMaterials(const aiScene * scene)
{
	textureList.resize(scene->mNumMaterials);

	// Loop through all the materials
	for (auto i = 0; i < scene->mNumMaterials; i++)
	{
		aiMaterial* material = scene->mMaterials[i];

		textureList[i] = nullptr;

		// Check if the material has a Diffuse
		if (material->GetTextureCount(aiTextureType_DIFFUSE))
		{
			aiString path;
			// Get the path of the Diffuse
			if (material->GetTexture(aiTextureType_DIFFUSE, 0, &path) == AI_SUCCESS)
			{	
				// To avoid absolute paths
				int idx = std::string(path.data).rfind("\\");
				std::string filename = std::string(path.data).substr(idx + 1);
				
				// For it to work with project filepath
				std::string texPath = std::string("Textures/") + filename;

				// Set the path 
				textureList[i] = new Texture(texPath.c_str());

				// Check if the texture loaded correctly
				if (!textureList[i]->LoadTextureAlpha())
				{
					printf("Failed to load texture at: %s\n", texPath);
					delete textureList[i];
					textureList[i] = nullptr;
				}
			}
		}

		if (!textureList[i])
		{
			textureList[i] = new Texture("Textures/plain.png");
			textureList[i]->LoadTextureAlpha();
		}
	}
}


