#define STB_IMAGE_IMPLEMENTATION

#include <stdio.h>
#include <string.h>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <ctime>

#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "CommonValues.h"

#include "GLWindow.h"
#include "Mesh.h"
#include "Shader.h"
#include "OverlayShader.h"
#include "TextShader.h"
#include "Camera.h"
#include "Texture.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "SpotLight.h"
#include "Material.h"
#include "Model.h"
#include "Skybox.h"

GLWindow mainWindow;
std::vector<Mesh*> meshList;
std::vector<Shader> shaderList;
OverlayShader* overlayShader;
TextShader* textShader;
Camera camera;

Material shinyMaterial;
Material dullMaterial;

std::vector<Model> models;
std::map<std::string, Model*> treasureMap;

DirectionalLight mainLight;
PointLight pointLights[MAX_POINT_LIGHTS];
SpotLight spotLights[MAX_SPOT_LIGHTS];

std::vector<glm::vec3> hidingCoordinates { 
	glm::vec3(5.99f, 12.09f, 75.76f), 
	glm::vec3(26.41f, 12.56f, 113.99f), 
	glm::vec3(40.86f, 12.97f, 102.51f),
	glm::vec3(69.66f, 3.23f, 72.92f),
	glm::vec3(56.921f, 11.28f, 91.60f),
	glm::vec3(14.57f, 0.0f, 64.92f),
	glm::vec3(22.59, 0.0f, 107.02f),
	glm::vec3(82.31f, 0.0f, 101.27f),
	glm::vec3(56.68f, 14.21f, 113.186f),
};

Skybox skybox;
Texture* overlayTexture;

GLfloat deltaTime = 0.0f;
GLfloat lastTime = 0.0f;
GLfloat elapsedTime = 0.0f;
GLuint uniformModel = 0;

bool showOverlay = true;

void CreateShaders();
void CreateOverlayShaders();
void CreateTextShaders();
void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
void RenderOverlayTexture();
void renderTheTreasureModels();
void renderModels();
void renderText();

int main()
{
	srand((int)time(0));

	mainWindow = GLWindow(1366, 768); 
	mainWindow.init();

	CreateShaders();
	CreateOverlayShaders();
	CreateTextShaders();

	// Instantiate a camera: (startposition, globalUpValue, Rotation on Y axis, Rotation on the Z axis, CameraMovespeed, cameraRotationSpeed)
	camera = Camera(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), -60.0f, 0.0f, 5.0f, 0.5f);

	shinyMaterial = Material(4.0f, 256);
	dullMaterial = Material(0.3f, 4);
	
	int r;
	Model scene = Model();
	scene.LoadModel("Models/scene.obj");
	*scene.Position = glm::vec3(0.0f, -2.0f, 0.0f);
	models.push_back(scene);

	r = (rand() % hidingCoordinates.size());
	Model treasure1 = Model();
	treasure1.LoadModel("Models/coins.obj");
	*treasure1.Position = hidingCoordinates[r];
	*treasure1.Scale = glm::vec3(0.5f, 0.5f, 0.5f);
	treasureMap.insert(std::pair<std::string, Model*>("A", &treasure1));
	hidingCoordinates.erase(hidingCoordinates.begin() + r);

	r = (rand() % hidingCoordinates.size());
	Model treasure2 = Model();
	treasure2.LoadModel("Models/bluegem.obj");
	*treasure2.Position = hidingCoordinates[r];
	treasureMap.insert(std::pair<std::string, Model*>("B", &treasure2));
	hidingCoordinates.erase(hidingCoordinates.begin() + r);

	r = (rand() % hidingCoordinates.size());
	Model treasure3 = Model();
	treasure3.LoadModel("Models/pinkgem.obj");
	*treasure3.Position = hidingCoordinates[r];
	treasureMap.insert(std::pair<std::string, Model*>("C", &treasure3));
	hidingCoordinates.erase(hidingCoordinates.begin() + r);

	r = (rand() % hidingCoordinates.size());
	Model treasure4 = Model();
	treasure4.LoadModel("Models/greengem.obj");
	*treasure4.Position = hidingCoordinates[r];
	treasureMap.insert(std::pair<std::string, Model*>("D", &treasure4));
	hidingCoordinates.erase(hidingCoordinates.begin() + r);

	r = (rand() % hidingCoordinates.size());
	Model treasure5 = Model();
	treasure5.LoadModel("Models/chest.obj");
	*treasure5.Position = hidingCoordinates[r];
	treasureMap.insert(std::pair<std::string, Model*>("E", &treasure5));
	hidingCoordinates.erase(hidingCoordinates.begin() + r);


	//Insatiate scene main light: (r, g, b, aintensity, dintensity, dir.x, dir.y, dir.z) 
	mainLight = DirectionalLight(1.0f, 1.0f, 1.0f, 0.18f, 0.1f, 0.0f, 0.0f, -1.0f);

	// White light point light
	unsigned int pointLightCount = 0;
	pointLights[0] = PointLight(1.0f, 1.0f, 1.0f, 0.0f, 0.1f, 87.0f, 23.0f, 74.0f, 1.0f, 0.0005f, 0.0001f);
	pointLightCount++;
	pointLights[1] = PointLight(1.0f, 1.0f, 1.0f, 0.0f, 0.2f, 87.0f, 23.0f, 74.0f, 1.0f, 0.05f, 0.05f);
	pointLightCount++;
	pointLights[2] = PointLight(1.0f, 1.0f, 1.0f, 0.0f, 0.07f, 138.0f, 27.0f, 34.0f, 1.0f, 0.00001f, 0.00001f);
	pointLightCount++;

	// Light for objects
	pointLights[3] = PointLight(1.0f, 1.0f, 1.0f, 0.0f, 1.0f, treasure1.Position->x, treasure1.Position->y, treasure1.Position->z, 1.0f, 1.0f, 0.1f);
	pointLightCount++;
	pointLights[4] = PointLight(0.1f, 1.0f, 0.1f, 0.0f, 1.0f, treasure2.Position->x, treasure2.Position->y, treasure2.Position->z, 1.0f, 1.0f, 0.1f);
	pointLightCount++;
	pointLights[5] = PointLight(0.75f, 0.1f, 1.0f, 0.0f, 1.0f, treasure3.Position->x, treasure3.Position->y, treasure3.Position->z, 1.0f, 1.0f, 0.1f);
	pointLightCount++;
	pointLights[6] = PointLight(0.1f, 0.1f, 1.0f, 0.0f, 1.0f, treasure4.Position->x, treasure4.Position->y, treasure4.Position->z, 1.0f, 1.0f, 0.1f);
	pointLightCount++;
	pointLights[7] = PointLight(1.0f, 1.0f, 1.0f, 0.0f, 1.0f, treasure5.Position->x, treasure5.Position->y, treasure5.Position->z, 1.0f, 1.0f, 0.1f);
	pointLightCount++;

	unsigned int spotLightCount = 0;
	// Spotlight attached to camera
	spotLights[0] = SpotLight(1.0f, 1.0f, 1.0f, 0.0f, 2.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 20.0f);
	spotLightCount++;

	// skybox textures
	std::vector<std::string> skyboxFaces;
	skyboxFaces.push_back("Textures/Skybox/hills_rt.tga");
	skyboxFaces.push_back("Textures/Skybox/hills_lf.tga");
	skyboxFaces.push_back("Textures/Skybox/hills_up.tga");
	skyboxFaces.push_back("Textures/Skybox/hills_dn.tga");
	skyboxFaces.push_back("Textures/Skybox/hills_bk.tga");
	skyboxFaces.push_back("Textures/Skybox/hills_ft.tga");

	// create the Skybox
	skybox = Skybox(skyboxFaces);

	// texture of the castle from the top
	overlayTexture = new Texture("Textures/map.png");
	if (!overlayTexture->LoadTexture())
	{
		printf("Failed to load overlay texture");
		delete overlayTexture;
		overlayTexture = nullptr;
	}

	//Declare uniform values
	GLuint uniformProjection = 0, uniformView = 0, uniformEyePosition = 0,
		uniformSpecularIntensity = 0, uniformShininess = 0;

	//Initialize projection matrix 
	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)mainWindow.getBufferWidth() / mainWindow.getBufferHeight(), 0.1f, 100.0f);

	// Do until window is closed
	while (!mainWindow.getShouldClose())
	{
		//Calculate deltaTime 
		GLfloat currentTime = glfwGetTime();
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		// Get and Handle User Input
		glfwPollEvents();

		camera.getKey(mainWindow.getsKeys(), deltaTime);
		camera.getMouse(mainWindow.getXMovement(), mainWindow.getYMovement());

		// Clear the window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Draw Sky box
		skybox.drawSkybox(camera.calculateViewMatrix(), projection);

		shaderList[0].UseShader();
		// Get uniforms locations
		uniformModel = shaderList[0].GetModelLocation();
		uniformProjection = shaderList[0].GetProjectionLocation();
		uniformView = shaderList[0].GetViewLocation();
		uniformEyePosition = shaderList[0].GetEyePositionLocation();
		uniformSpecularIntensity = shaderList[0].GetSpecularIntensityLocation();
		uniformShininess = shaderList[0].GetShininessLocation();

		// Lower get camera Position and lower it a bit so as to get a better effect
		glm::vec3 lowerLight = camera.getCameraPosition();
		lowerLight.y -= 0.3f;
		// Handle flashlight input
		spotLights[0].flashLightGetKeys(mainWindow.getsKeys(), deltaTime);
		// Attach light to our main camera
		spotLights[0].SetFlash(lowerLight, camera.getCameraDirection());

		shaderList[0].SetDirectionalLight(&mainLight);
		shaderList[0].SetPointLights(pointLights, pointLightCount);
		shaderList[0].SetSpotLights(spotLights, spotLightCount);

		// set view and projection uniforms
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(camera.calculateViewMatrix()));
		glUniform3f(uniformEyePosition, camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);


		// excludes the 5 collectible objects
		renderModels();
		
		// render treasures
		renderTheTreasureModels();
		

		// draw overlay elements
		if (showOverlay && mainWindow.getsKeys()[GLFW_KEY_SPACE]) {
			showOverlay = false;
			elapsedTime = 0.0f;
		}
		elapsedTime += deltaTime;
		if (showOverlay) {
			RenderOverlayTexture();
		}

		// render the text
		renderText();
		
		glUseProgram(0);

		mainWindow.swapBuffers();
	}

	return 0;
}

void renderModels() {
	for (int o = 0; o < models.size(); ++o) {
		Model obj = models[o];
		glm::mat4 model = glm::mat4();
		model = glm::rotate(model, (*obj.Rotation).x, glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, (*obj.Rotation).y, glm::vec3(0.0, 1.0, 0.0));
		model = glm::rotate(model, (*obj.Rotation).z, glm::vec3(0.0, 0.0, 1.0));
		model = glm::translate(model, *obj.Position);
		model = glm::scale(model, *obj.Scale);
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		//shinyMaterial.UseMaterial(uniformSpecularIntensity, uniformShininess);
		obj.RenderModel();

	}
}

void renderTheTreasureModels() {
	for (auto obj : treasureMap)
	{
		if (obj.second == nullptr) {
			continue;
		}

		glm::mat4 model = glm::mat4();
		model = glm::rotate(model, (*obj.second->Rotation).x, glm::vec3(1.0, 0.0, 0.0));
		model = glm::rotate(model, (*obj.second->Rotation).y, glm::vec3(0.0, 1.0, 0.0));
		model = glm::rotate(model, (*obj.second->Rotation).z, glm::vec3(0.0, 0.0, 1.0));
		model = glm::translate(model, *obj.second->Position);
		model = glm::scale(model, *obj.second->Scale);

		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		//shinyMaterial.UseMaterial(uniformSpecularIntensity, uniformShininess);
		obj.second->RenderModel();

		//check distances from player camera
		float distance = glm::distance(*obj.second->Position, camera.getCameraPosition());
		if (distance < 1.5f) {
			treasureMap.at(obj.first) = nullptr;
		}
	}
}
void renderText() {
	int treasureIndex = 0;
	for (auto obj : treasureMap)
	{
		if (treasureMap.at(obj.first) == nullptr) {
			RenderText(obj.first, 500.0f + (100.0f * treasureIndex), 25.0f, 1.0f, glm::vec3(1, 0.5f, 0.5f));
		}
		else {
			RenderText(obj.first, 500.0f + (100.0f * treasureIndex), 25.0f, 1.0f, glm::vec3(0.5f, 1, 0.5f));
		}
		++treasureIndex;
		if (treasureIndex == treasureMap.size()) {
			treasureIndex = 0;
		}
	}
	RenderText("Time: " + std::to_string(int(elapsedTime)), 25.0f, 25.0f, 1.0f, glm::vec3(1, 1, 1));
}
void CreateShaders()
{
	Shader *shader1 = new Shader();
	shader1->CreateFromFiles("Shaders/shader.vert", "Shaders/shader.frag");
	shaderList.push_back(*shader1);
}


void CreateOverlayShaders()
{
	overlayShader = new OverlayShader();
	overlayShader->CreateFromFiles("Shaders/overlayshader.vert", "Shaders/overlayshader.frag");
}

void CreateTextShaders()
{
	textShader = new TextShader();
	textShader->CreateFromFiles("Shaders/textshader.vert", "Shaders/textshader.frag");
}

void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Activate corresponding render state	
	textShader->UseShader();
	glUniform3f(textShader->GetTextColorLocation(), color.x, color.y, color.z);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(textShader->VAO);

	// Iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		Character ch = textShader->Characters[*c];

		GLfloat xpos = x + ch.Bearing.x * scale;
		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

		GLfloat w = ch.Size.x * scale;
		GLfloat h = ch.Size.y * scale;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, textShader->VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void RenderOverlayTexture()
{
	// Activate corresponding render state	
	overlayShader->UseShader();

	overlayTexture->UseTexture();
	glBindVertexArray(overlayShader->VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}