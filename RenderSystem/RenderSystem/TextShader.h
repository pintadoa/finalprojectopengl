#pragma once

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>


#include <stdio.h>
#include <string>
#include <map>
#include <iostream>
#include <fstream>

// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H

#include <GL\glew.h>

#include "CommonValues.h"
#include "Character.h"

#include "DirectionalLight.h"
#include "PointLight.h"
#include "SpotLight.h"

class TextShader
{
public:
	TextShader();
	~TextShader();

	void CreateFromFiles(const char* vertexLocation, const char* fragmentLocation);

	std::string ReadFile(const char* fileLocation);

	GLuint GetProjectionLocation() { return uniformProjection; }
	GLuint GetTextureLocation() { return uniformProjection; }
	GLuint GetTextColorLocation() { return uniformTextColor; }

	void UseShader();
	void ClearShader();
	GLuint VAO, VBO;
	std::map<GLchar, Character> Characters;

private:
	GLuint shaderID, uniformProjection, uniformTextColor, uniformTexture;

	glm::mat4 projection;

	void CompileShader(const char* vertexCode, const char* fragmentCode);
	void AddShader(GLuint theProgram, const char* shaderCode, GLenum shaderType);
};

