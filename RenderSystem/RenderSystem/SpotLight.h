#pragma once
#include "PointLight.h"
#include <GLFW\glfw3.h>

class SpotLight :
	public PointLight
{
public:
	SpotLight();
	SpotLight(GLfloat red, GLfloat green, GLfloat blue,
		GLfloat aIntensity, GLfloat dIntensity,
		GLfloat xPos, GLfloat yPos, GLfloat zPos,
		GLfloat xDir, GLfloat yDir, GLfloat zDir,
		GLfloat constant, GLfloat linear, GLfloat exponent,
		GLfloat edge);
	~SpotLight();

	void UseLight(GLuint ambientIntensityLocation, GLuint ambientColourLocation,
		GLuint diffuseIntensityLocation, GLuint positionLocation, GLuint directionLocation,
		GLuint constantLocation, GLuint linearLocation, GLuint exponentLocation,
		GLuint edgeLocation);

	void SetFlash(glm::vec3 pos, glm::vec3 dir); // Set spotlight to camera position and direction

	void flashLightGetKeys(bool* keys, GLfloat deltaTime); 

private:
	// Direction its pointing
	glm::vec3 direction;

	// Angle of edge and the procesed edge
	GLfloat edge, procEdge;
};

