#pragma once

#include "stdio.h"

#include <GL\glew.h>
#include <GLFW\glfw3.h>

class GLWindow
{
public:
	GLWindow();
	GLWindow(GLint windowWidth, GLint windowHeight);
	~GLWindow();

	int init();
	GLfloat getXMovement();// Get mouse movement in X
	GLfloat getYMovement();// Get mouse movement in Y


	GLint getBufferWidth() { return bufferWidth; }
	GLint getBufferHeight() { return bufferHeight; }
	bool* getsKeys() { return keys; }
	bool getShouldClose() { return glfwWindowShouldClose(mainWindow); }
	void swapBuffers() { glfwSwapBuffers(mainWindow); }

private:
	GLFWwindow* mainWindow;

	GLint width, height;
	GLint bufferWidth, bufferHeight;

	bool keys[1024];

	GLfloat lastX;
	GLfloat lastY;
	GLfloat xMovement;
	GLfloat yMovement;
	bool mouseFirstMoved;

	void createCallbacks();
	static void handleKeysPress(GLFWwindow* window, int key, int code, int action, int mode);
	static void handleMouseMovement(GLFWwindow* window, double xPos, double yPos);
};