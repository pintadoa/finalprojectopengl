#pragma once

#include <GL\glew.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <GLFW\glfw3.h>


class Camera
{
public:
	Camera();
	Camera(glm::vec3 startPosition, glm::vec3 startUp, GLfloat startYaw, GLfloat startPitch, GLfloat startMoveSpeed, GLfloat startTurnSpeed);
	~Camera();

	void getKey(bool* keys, GLfloat deltaTime);
	void getMouse(GLfloat xMovement, GLfloat yMovement);

	// Return the camera position
	glm::vec3 getCameraPosition() { return position; }

	// Return the camera direction
	glm::vec3 getCameraDirection() { return glm::normalize(front); };

	// Calculate the current view matrix based
	glm::mat4 calculateViewMatrix() { return glm::lookAt(position, position + front, up); }

private:
	GLfloat yaw;// Rotation on Y axis 
	GLfloat pitch;// Rotation on the Z axis 

	glm::vec3 position; // Camera position
	glm::vec3 front; // Front of Camera
	glm::vec3 up; // Up of Camera
	glm::vec3 right; // Right of Camera
	glm::vec3 worldUp; // World Camera Up

	//Movement speed 
	GLfloat moveSpeed;
	GLfloat turnSpeed;
};

